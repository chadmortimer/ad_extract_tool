﻿namespace ADExtractTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_BaseOU = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_OU = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.txt_filter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_busy = new System.Windows.Forms.Label();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.lst_log = new System.Windows.Forms.ListBox();
            this.txt_attr = new System.Windows.Forms.TextBox();
            this.btn_clear_att = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.rdbtn_list = new System.Windows.Forms.RadioButton();
            this.rdbtn_select = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txt_limit = new System.Windows.Forms.ComboBox();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.rdbtn_computers = new System.Windows.Forms.RadioButton();
            this.rdbtn_groups = new System.Windows.Forms.RadioButton();
            this.rdbtn_users = new System.Windows.Forms.RadioButton();
            this.button3 = new System.Windows.Forms.Button();
            this.lbl_scope = new System.Windows.Forms.Label();
            this.btn_copy_attr = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rdbtn_export_csv = new System.Windows.Forms.RadioButton();
            this.rdbtn_export_tab = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_BaseOU
            // 
            this.txt_BaseOU.Location = new System.Drawing.Point(81, 12);
            this.txt_BaseOU.Name = "txt_BaseOU";
            this.txt_BaseOU.Size = new System.Drawing.Size(249, 20);
            this.txt_BaseOU.TabIndex = 0;
            this.txt_BaseOU.Text = "DC=za,DC=Kworld,DC=KPMG,DC=com";
            this.txt_BaseOU.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Domain";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Search Scope - Select Node";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btn_OU
            // 
            this.btn_OU.Location = new System.Drawing.Point(336, 10);
            this.btn_OU.Name = "btn_OU";
            this.btn_OU.Size = new System.Drawing.Size(75, 23);
            this.btn_OU.TabIndex = 4;
            this.btn_OU.Text = "search";
            this.btn_OU.UseVisualStyleBackColor = true;
            this.btn_OU.Click += new System.EventHandler(this.btn_OU_Click);
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(81, 287);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(330, 146);
            this.listView1.TabIndex = 5;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // txt_filter
            // 
            this.txt_filter.Location = new System.Drawing.Point(439, 150);
            this.txt_filter.Name = "txt_filter";
            this.txt_filter.Size = new System.Drawing.Size(291, 20);
            this.txt_filter.TabIndex = 6;
            this.txt_filter.Text = "(&(objectClass=user))";
            this.txt_filter.TextChanged += new System.EventHandler(this.txt_filter_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(436, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Filter (optional)";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(213, 480);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 485);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Export limit";
            // 
            // lbl_busy
            // 
            this.lbl_busy.AutoSize = true;
            this.lbl_busy.Location = new System.Drawing.Point(404, 487);
            this.lbl_busy.Name = "lbl_busy";
            this.lbl_busy.Size = new System.Drawing.Size(0, 13);
            this.lbl_busy.TabIndex = 12;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(309, 480);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.TabIndex = 13;
            this.btn_cancel.Text = "Cancel Export";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.button2_Click);
            // 
            // lst_log
            // 
            this.lst_log.FormattingEnabled = true;
            this.lst_log.Location = new System.Drawing.Point(90, 509);
            this.lst_log.Name = "lst_log";
            this.lst_log.Size = new System.Drawing.Size(691, 134);
            this.lst_log.TabIndex = 14;
            // 
            // txt_attr
            // 
            this.txt_attr.Location = new System.Drawing.Point(450, 287);
            this.txt_attr.Multiline = true;
            this.txt_attr.Name = "txt_attr";
            this.txt_attr.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_attr.Size = new System.Drawing.Size(331, 146);
            this.txt_attr.TabIndex = 15;
            // 
            // btn_clear_att
            // 
            this.btn_clear_att.Location = new System.Drawing.Point(706, 439);
            this.btn_clear_att.Name = "btn_clear_att";
            this.btn_clear_att.Size = new System.Drawing.Size(75, 23);
            this.btn_clear_att.TabIndex = 18;
            this.btn_clear_att.Text = "Clear";
            this.btn_clear_att.UseVisualStyleBackColor = true;
            this.btn_clear_att.Click += new System.EventHandler(this.btn_clear_att_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.rdbtn_list);
            this.panel1.Controls.Add(this.rdbtn_select);
            this.panel1.Location = new System.Drawing.Point(81, 249);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(700, 32);
            this.panel1.TabIndex = 19;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(117, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 23);
            this.button2.TabIndex = 20;
            this.button2.Text = "Update Attributes";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // rdbtn_list
            // 
            this.rdbtn_list.AutoSize = true;
            this.rdbtn_list.Location = new System.Drawing.Point(378, 7);
            this.rdbtn_list.Name = "rdbtn_list";
            this.rdbtn_list.Size = new System.Drawing.Size(226, 17);
            this.rdbtn_list.TabIndex = 19;
            this.rdbtn_list.TabStop = true;
            this.rdbtn_list.Text = "Specify list of attributes (1 line per attribute)";
            this.rdbtn_list.UseVisualStyleBackColor = true;
            // 
            // rdbtn_select
            // 
            this.rdbtn_select.AutoSize = true;
            this.rdbtn_select.Checked = true;
            this.rdbtn_select.Location = new System.Drawing.Point(9, 7);
            this.rdbtn_select.Name = "rdbtn_select";
            this.rdbtn_select.Size = new System.Drawing.Size(102, 17);
            this.rdbtn_select.TabIndex = 18;
            this.rdbtn_select.TabStop = true;
            this.rdbtn_select.Text = "Select Attributes";
            this.rdbtn_select.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(437, 176);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(293, 13);
            this.textBox1.TabIndex = 20;
            this.textBox1.Text = "e.g. (&(objectClass=user)(objectCategory=person))";
            // 
            // txt_limit
            // 
            this.txt_limit.FormattingEnabled = true;
            this.txt_limit.Items.AddRange(new object[] {
            "All Records",
            "10",
            "100",
            "250"});
            this.txt_limit.Location = new System.Drawing.Point(90, 480);
            this.txt_limit.Name = "txt_limit";
            this.txt_limit.Size = new System.Drawing.Size(121, 21);
            this.txt_limit.TabIndex = 21;
            // 
            // treeView2
            // 
            this.treeView2.Location = new System.Drawing.Point(81, 57);
            this.treeView2.Name = "treeView2";
            this.treeView2.Size = new System.Drawing.Size(330, 159);
            this.treeView2.TabIndex = 22;
            this.treeView2.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView2_AfterSelect);
            this.treeView2.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView2_NodeMouseClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.rdbtn_computers);
            this.panel2.Controls.Add(this.rdbtn_groups);
            this.panel2.Controls.Add(this.rdbtn_users);
            this.panel2.Location = new System.Drawing.Point(437, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(344, 63);
            this.panel2.TabIndex = 23;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Object Class";
            // 
            // rdbtn_computers
            // 
            this.rdbtn_computers.AutoSize = true;
            this.rdbtn_computers.Location = new System.Drawing.Point(173, 29);
            this.rdbtn_computers.Name = "rdbtn_computers";
            this.rdbtn_computers.Size = new System.Drawing.Size(75, 17);
            this.rdbtn_computers.TabIndex = 2;
            this.rdbtn_computers.Tag = "computer";
            this.rdbtn_computers.Text = "Computers";
            this.rdbtn_computers.UseVisualStyleBackColor = true;
            this.rdbtn_computers.CheckedChanged += new System.EventHandler(this.rdbtn_CheckedChanged);
            // 
            // rdbtn_groups
            // 
            this.rdbtn_groups.AutoSize = true;
            this.rdbtn_groups.Location = new System.Drawing.Point(97, 29);
            this.rdbtn_groups.Name = "rdbtn_groups";
            this.rdbtn_groups.Size = new System.Drawing.Size(59, 17);
            this.rdbtn_groups.TabIndex = 1;
            this.rdbtn_groups.Tag = "group";
            this.rdbtn_groups.Text = "Groups";
            this.rdbtn_groups.UseVisualStyleBackColor = true;
            this.rdbtn_groups.CheckedChanged += new System.EventHandler(this.rdbtn_CheckedChanged);
            // 
            // rdbtn_users
            // 
            this.rdbtn_users.AutoSize = true;
            this.rdbtn_users.Checked = true;
            this.rdbtn_users.Location = new System.Drawing.Point(22, 29);
            this.rdbtn_users.Name = "rdbtn_users";
            this.rdbtn_users.Size = new System.Drawing.Size(52, 17);
            this.rdbtn_users.TabIndex = 0;
            this.rdbtn_users.TabStop = true;
            this.rdbtn_users.Tag = "user";
            this.rdbtn_users.Text = "Users";
            this.rdbtn_users.UseVisualStyleBackColor = true;
            this.rdbtn_users.CheckedChanged += new System.EventHandler(this.rdbtn_CheckedChanged);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(736, 147);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(45, 23);
            this.button3.TabIndex = 24;
            this.button3.Text = "Clear";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // lbl_scope
            // 
            this.lbl_scope.AutoSize = true;
            this.lbl_scope.Location = new System.Drawing.Point(78, 219);
            this.lbl_scope.Name = "lbl_scope";
            this.lbl_scope.Size = new System.Drawing.Size(81, 13);
            this.lbl_scope.TabIndex = 25;
            this.lbl_scope.Text = "Selected Node:";
            this.lbl_scope.Click += new System.EventHandler(this.label6_Click_1);
            // 
            // btn_copy_attr
            // 
            this.btn_copy_attr.Location = new System.Drawing.Point(418, 288);
            this.btn_copy_attr.Name = "btn_copy_attr";
            this.btn_copy_attr.Size = new System.Drawing.Size(26, 23);
            this.btn_copy_attr.TabIndex = 26;
            this.btn_copy_attr.Text = ">";
            this.btn_copy_attr.UseVisualStyleBackColor = true;
            this.btn_copy_attr.Click += new System.EventHandler(this.btn_copy_attr_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rdbtn_export_csv);
            this.panel3.Controls.Add(this.rdbtn_export_tab);
            this.panel3.Location = new System.Drawing.Point(90, 439);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(455, 32);
            this.panel3.TabIndex = 27;
            // 
            // rdbtn_export_csv
            // 
            this.rdbtn_export_csv.AutoSize = true;
            this.rdbtn_export_csv.Location = new System.Drawing.Point(152, 8);
            this.rdbtn_export_csv.Name = "rdbtn_export_csv";
            this.rdbtn_export_csv.Size = new System.Drawing.Size(46, 17);
            this.rdbtn_export_csv.TabIndex = 19;
            this.rdbtn_export_csv.TabStop = true;
            this.rdbtn_export_csv.Text = "CSV";
            this.rdbtn_export_csv.UseVisualStyleBackColor = true;
            // 
            // rdbtn_export_tab
            // 
            this.rdbtn_export_tab.AutoSize = true;
            this.rdbtn_export_tab.Checked = true;
            this.rdbtn_export_tab.Location = new System.Drawing.Point(11, 8);
            this.rdbtn_export_tab.Name = "rdbtn_export_tab";
            this.rdbtn_export_tab.Size = new System.Drawing.Size(124, 17);
            this.rdbtn_export_tab.TabIndex = 18;
            this.rdbtn_export_tab.TabStop = true;
            this.rdbtn_export_tab.Text = "Tab delimited text file";
            this.rdbtn_export_tab.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 449);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Export Type";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(856, 726);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btn_copy_attr);
            this.Controls.Add(this.lbl_scope);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.treeView2);
            this.Controls.Add(this.txt_limit);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_clear_att);
            this.Controls.Add(this.txt_attr);
            this.Controls.Add(this.lst_log);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.lbl_busy);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_filter);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.btn_OU);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_BaseOU);
            this.Name = "Form1";
            this.Text = "Ad Export Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txt_BaseOU;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_OU;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.TextBox txt_filter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbl_busy;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.ListBox lst_log;
        private System.Windows.Forms.TextBox txt_attr;
        private System.Windows.Forms.Button btn_clear_att;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdbtn_list;
        private System.Windows.Forms.RadioButton rdbtn_select;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox txt_limit;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rdbtn_computers;
        private System.Windows.Forms.RadioButton rdbtn_groups;
        private System.Windows.Forms.RadioButton rdbtn_users;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lbl_scope;
        private System.Windows.Forms.Button btn_copy_attr;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rdbtn_export_csv;
        private System.Windows.Forms.RadioButton rdbtn_export_tab;
        private System.Windows.Forms.Label label6;
    }
}


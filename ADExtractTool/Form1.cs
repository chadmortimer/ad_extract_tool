﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Threading;

namespace ADExtractTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Control.CheckForIllegalCrossThreadCalls = false;

            backgroundWorker1 = new BackgroundWorker();

            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;

            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            backgroundWorker1.ProgressChanged += backgroundWorker1_ProgressChanged;
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;

            backgroundWorker2 = new BackgroundWorker();

            backgroundWorker2.WorkerReportsProgress = true;
            backgroundWorker2.WorkerSupportsCancellation = true;

            backgroundWorker2.DoWork += backgroundWorker2_DoWork;
            backgroundWorker2.ProgressChanged += backgroundWorker2_ProgressChanged;
            backgroundWorker2.RunWorkerCompleted += backgroundWorker2_RunWorkerCompleted;

            FileInfo fi = new FileInfo(Properties.Settings.Default.attr_file);
            if (fi.Exists)
            {

                string line;
                txt_attr.Text = "";
                // Read the file and display it line by line.  
                System.IO.StreamReader file =
                    new System.IO.StreamReader(Properties.Settings.Default.attr_file);

                while ((line = file.ReadLine()) != null)
                {
                    if (line.Trim() != "")
                        txt_attr.AppendText(line.Trim().TrimStart('\'') + Environment.NewLine);
                }

                file.Close();


            }

            txt_BaseOU.Text = Properties.Settings.Default.base_ou;
            txt_limit.SelectedIndex = 0;
            this.Update();
            treeView2.AfterExpand += treeView2_AfterExpand;

            this.backgroundWorker2.RunWorkerAsync();

           // updateAttr();
        }

        private void treeView2_AfterExpand(object sender, TreeViewEventArgs e)
        {

            this.backgroundWorker2.RunWorkerAsync(e.Node);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btn_OU_Click(object sender, EventArgs e)
        {
            //getOUs();

            treeView2.Nodes.Clear();

            this.backgroundWorker2.RunWorkerAsync();
        }



        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void lst_OU_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lst_properties_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txt_filter_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private BackgroundWorker backgroundWorker1, backgroundWorker2;
        SaveFileDialog saveFileDialog1;
        TimeSpan ts;
        int totalrecords = 0;
        private void button1_Click(object sender, EventArgs e)
        {

            // Displays a SaveFileDialog so the user can save the Image  
            // assigned to Button2.  
            saveFileDialog1 = new SaveFileDialog();

            if (rdbtn_export_csv.Checked)
            {
                saveFileDialog1.Filter = "CSV|*.csv";
                saveFileDialog1.Title = "Export CSV File";
            }
            else
            {
                saveFileDialog1.Filter = "Text|*.txt";
                saveFileDialog1.Title = "Export TAB delimited Text File";
            }

            saveFileDialog1.ShowDialog();

            if (treeView2.SelectedNode != null)
            {
                if (saveFileDialog1.FileName != "")
                {
                    FileInfo fl = new FileInfo(saveFileDialog1.FileName);
                    lst_log.Items.Clear();

                    if (!IsFileLocked(fl) || !fl.Exists)
                    {

                        foreach (Control ctrl in this.Controls)
                        {
                            if (ctrl.Name != "btn_cancel")
                                ctrl.Enabled = false;
                        }


                        lbl_busy.Text = "Exporting....";

                        this.Update();

                        ts = new TimeSpan();

                        this.backgroundWorker1.RunWorkerAsync();
                    }
                    else
                    {
                        MessageBox.Show("File cannot be access - in use by another process");
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select the search scope from the tree");
            }

        }


        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        // This event handler is where the time-consuming work is done.
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            var cn = "";
            var prop = "";
            var val = "";
            var check = listView1.CheckedItems;

            var delimeter = "\t";
            
            if(rdbtn_export_csv.Checked)
            {
                delimeter = ",";
            }

            List<string> selected = new List<string>() { };

            if (rdbtn_select.Checked)
            {
                foreach (ListViewItem sel in check)
                {
                    selected.Add(sel.Text);
                }
            }
            else
            {
                using (var w = new StreamWriter("attributes_saved.txt", false))
                {
                    foreach (var row in txt_attr.Lines)
                    {
                        if (row.Trim() != "")
                        {
                            selected.Add(row.Trim().TrimStart('\''));

                            w.WriteLine(row.Trim().TrimStart('\''));
                        }
                    }
                }
            }


            if (treeView2.SelectedNode != null)
            {


                // If the file name is not an empty string open it for saving.  
                if (saveFileDialog1.FileName != "")
                {
                    var node = treeView2.SelectedNode;
                    var path = "";

                    if (node.Tag.GetType() == typeof(SearchResult))
                    {
                        path = ((SearchResult)node.Tag).Properties["adspath"][0].ToString();
                    }
                    else
                    {
                        path = node.Tag.ToString();
                    }

                    var pSearcher = new DirectorySearcher(new DirectoryEntry(path));

                    pSearcher.PageSize = 1000;

                    //string filter = "(&(objectClass=user)(objectCategory=person)(c=za))";
                    pSearcher.Filter = txt_filter.Text;
                    pSearcher.Sort.PropertyName = "sn";

                    var limit = 0;

                    if (int.TryParse(txt_limit.Text, out limit))
                    {
                        pSearcher.SizeLimit = limit;

                        if (limit % 1000 == 0)
                        {
                            pSearcher.PageSize = 999;
                        }
                    }

                    using (var w = new StreamWriter(saveFileDialog1.FileName))
                    {
                        foreach (string property in selected)
                        {
                            w.Write(property + delimeter);
                        }
                        w.Write(Environment.NewLine);
                        int cnt = 1;

                        foreach (SearchResult sr in pSearcher.FindAll())
                        {

                            try
                            {

                                if (worker.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return;
                                }
                                cn = sr.Properties["cn"][0].ToString();

                                foreach (string property in selected)
                                {

                                    try
                                    {
                                        prop = property;

                                        if (sr.Properties[property].Count > 0)
                                        {
                                            var type = sr.Properties[property][0].GetType();
                                            val = sr.Properties[property][0].ToString();

                                            DateTime dt = new DateTime();

                                            if (type == typeof(Int64) && (Int64)sr.Properties[property][0] != 0 && (Int64)sr.Properties[property][0] != Int64.MaxValue)
                                            {
                                                dt = DateTime.FromFileTime((Int64)sr.Properties[property][0]);
                                                w.Write(string.Format("\"{0}\""+delimeter, dt.ToString()));
                                            }
                                            else
                                            {
                                                w.Write(string.Format("\"{0}\"" + delimeter, sr.Properties[property][0].ToString()));
                                            }

                                        }
                                        else
                                        {
                                            w.Write(string.Format("\"{0}\""+delimeter , ""));
                                        }
                                    }
                                    catch (Exception ee)
                                    {
                                        w.Write(string.Format("\"{0}\"" + delimeter, ""));
                                        lst_log.Items.Add(cnt + " - " + cn + " " + prop + " = " + val + " : " + ee.Message);
                                        this.Update();

                                    }

                                }

                                w.Write(Environment.NewLine);

                                lst_log.Items.Add(sr.Properties["cn"][0].ToString());

                            }
                            catch (Exception ex)
                            {
                                lst_log.Items.Add(cnt + " - " + cn + " " + prop + " = " + val + " : " + ex.Message);
                                this.Update();
                            }


                            backgroundWorker1.ReportProgress(cnt, cnt);
                            cnt++;

                        }




                    }

                }


            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // lbl_busy.Text = "Exporting.... " + (e.ProgressPercentage.ToString() + "%");
            lbl_busy.Text = "Exporting.... " + e.UserState.ToString();
            this.Update();
        }

        // This event handler deals with the results of the background operation.
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                lbl_busy.Text = "Canceled!";

                MessageBox.Show("Export Cancelled");

            }
            else if (e.Error != null)
            {
                lbl_busy.Text = "Error: " + e.Error.Message;
            }
            else
            {
                lbl_busy.Text = "";

                MessageBox.Show("Export Complete");
                /* run your code here */
                // Console.WriteLine("Hello, world");
            }

            foreach (Control ctrl in this.Controls)
            {
                ctrl.Enabled = true;
            }

            this.Update();
        }

        // This event handler is where the time-consuming work is done.
        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            treeView2.Enabled = false;

            BackgroundWorker worker = sender as BackgroundWorker;

            var loc = "LDAP://";
            TreeNode topnode = new TreeNode();

            if (e.Argument == null) //Means we are starting at top
            {
                loc += txt_BaseOU.Text;

                topnode.Tag = loc;
                topnode.Text = txt_BaseOU.Text;

                treeView2.Invoke((MethodInvoker)(() => treeView2.Nodes.Add(topnode)));
            }
            else if (e.Argument.GetType() == typeof(TreeNode))
            {
                TreeNode tv = e.Argument as TreeNode;
                if (tv.Tag.GetType() == typeof(SearchResult))
                {
                    loc = ((SearchResult)tv.Tag).Properties["adspath"][0].ToString();
                }
                else
                {
                    loc = tv.Tag.ToString();
                }

                tv.Nodes.Clear();
            }

          
            var pSearcher = new DirectorySearcher(new DirectoryEntry(loc));

            //pSearcher.PageSize = 1000;
            pSearcher.SearchScope = SearchScope.OneLevel;

            string filter = "(&objectCategory=organizationalUnit)";

            //pSearcher.Sort.PropertyName = "sn";
           // pSearcher.Filter = filtersn

            foreach (SearchResult sr in pSearcher.FindAll())
            {

                //try
                // {

                var adspath = sr.Properties["adspath"][0].ToString();

                //if (adspath != loc)
               // {
                    if (e.Argument == null)
                    {
                        TreeNode tn = new TreeNode();
                        tn.Tag = sr;
                        if(sr.Properties["name"].Count > 0)
                            tn.Text = sr.Properties["name"][0].ToString();
                        else 
                            tn.Text = sr.Properties["adspath"][0].ToString();

                        tn.Nodes.Add("...");

                        treeView2.Invoke((MethodInvoker)(() => topnode.Nodes.Add(tn)));
                    }
                    else if (e.Argument.GetType() == typeof(TreeNode))
                    {
                        TreeNode tv = e.Argument as TreeNode;
                        TreeNode tn = new TreeNode();
                    if (sr.Properties["name"].Count > 0)
                        tn.Text = sr.Properties["name"][0].ToString();
                    else
                        tn.Text = sr.Properties["adspath"][0].ToString();

                    tn.Tag = sr;
                        tn.Nodes.Add("...");
                        treeView2.Invoke((MethodInvoker)(() => tv.Nodes.Add(tn)));
                    }
               /* }
                else
                {
                    if (e.Argument == null)
                    {


                    }
                }*/


                // }
                // catch (Exception ex)
                // {
                //lst_log.Items.Add(cnt + " - " + cn + " " + prop + " = " + val + " : " + ex.Message);
                this.Update();
                // }


                // backgroundWorker2.ReportProgress(cnt, cnt);


            }




        }

        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // lbl_busy.Text = "Exporting.... " + (e.ProgressPercentage.ToString() + "%");
            lbl_busy.Text = "Exporting.... " + e.UserState.ToString();
            this.Update();
        }

        // This event handler deals with the results of the background operation.
        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                // lbl_busy.Text = "Canceled!";

                //  MessageBox.Show("Export Cancelled");

            }
            else if (e.Error != null)
            {
                // lbl_busy.Text = "Error: " + e.Error.Message;
            }
            else
            {
                //lbl_busy.Text = "";

                // MessageBox.Show("Export Complete");
                /* run your code here */
                // Console.WriteLine("Hello, world");
            }

            foreach (Control ctrl in this.Controls)
            {
                ctrl.Enabled = true;
            }

            treeView2.Enabled = true;
            this.Update();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void btn_clear_att_Click(object sender, EventArgs e)
        {
            txt_attr.Text = "";
        }

        private void updateAttr()
        {
            button2.Enabled = false;
            button2.Text = "Loading...";
            listView1.Items.Clear();
            listView1.Items.Add("loading...");
            listView1.Enabled = false;
            this.Update();

            var checkedButton = panel2.Controls.OfType<RadioButton>()
                                      .FirstOrDefault(r => r.Checked);

            if (treeView2.SelectedNode != null)
            {
                //MessageBox.Show(lst_OU.SelectedItem.ToString());

                var node = treeView2.SelectedNode;
                var path = "";

                if (node.Tag.GetType() == typeof(SearchResult))
                {
                    path = ((SearchResult)node.Tag).Properties["adspath"][0].ToString();
                }
                else
                {
                    path = node.Tag.ToString();
                }

                var pSearcher = new DirectorySearcher(new DirectoryEntry(path));
                pSearcher.PageSize = 1000;
                //string filter = "(&(objectClass=user)(objectCategory=person)(c=za))";
                pSearcher.Filter = "(&(objectClass=" + checkedButton.Tag + "))";
                pSearcher.Sort.PropertyName = "sn";

                var deUser = pSearcher.FindOne();

                if (deUser != null)
                {
                    listView1.Items.Clear();

                    // Set the view to show details.
                    listView1.View = View.Details;
                    // Allow the user to edit item text.
                    // Allow the user to rearrange columns.
                    listView1.AllowColumnReorder = true;
                    // Display check boxes.
                    listView1.CheckBoxes = true;
                    // Select the item and subitems when selection is made.
                    listView1.FullRowSelect = true;
                    // Display grid lines.
                    listView1.GridLines = true;
                    // Sort the items in the list in ascending order.
                    listView1.Sorting = SortOrder.Ascending;


                    // Create columns for the items and subitems.
                    // Width of -2 indicates auto-size.
                    listView1.Columns.Add("Attributes", -2, HorizontalAlignment.Left);

                    //Add the items to the ListView.
                    //  listView1.Items.AddRange(new ListViewItem[] { item1, item2, item3 });

                    foreach (string property in deUser.Properties.PropertyNames)
                    {
                        listView1.Items.Add(property);
                        listView1.FindItemWithText(property).Checked = true;
                    }
                }
                else
                {
                    MessageBox.Show("No attributes found for the selected object type/class and search scope");
                }

            }
            else
            {
                MessageBox.Show("Please select the search scope from the tree");
            }

            button2.Enabled = true;
            button2.Text = "Update Attributes";

            listView1.Enabled = true;
        }

    

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            updateAttr();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txt_filter.Text = "";
            this.Update();
        }

        private void rdbtn_CheckedChanged(object sender, EventArgs e)
        {

            RadioButton ck = sender as RadioButton;
            var type = ck.Tag;
            txt_filter.Text = $"(&(objectClass={type}))";
            //updateAttr();
        }

        private void label6_Click_1(object sender, EventArgs e)
        {

        }

        void ColorNode(TreeNodeCollection nodes)
        {

            foreach (TreeNode child in nodes)
            {
                
                if(child.IsSelected)
                {
                    child.ForeColor = System.Drawing.Color.MidnightBlue;
                    child.BackColor = System.Drawing.Color.PowderBlue;
                }
                else
                {
                    child.ForeColor = System.Drawing.Color.Black;
                    child.BackColor = System.Drawing.Color.White;
                }

                if (child.Nodes != null && child.Nodes.Count > 0)
                    ColorNode(child.Nodes);
            }
        }

        private void treeView2_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeView tv = (TreeView)sender;
            ColorNode(tv.Nodes);

            var node = tv.SelectedNode;
            var path = "";

            if (node.Tag.GetType() == typeof(SearchResult))
            {
                path = ((SearchResult)node.Tag).Properties["adspath"][0].ToString();
            }
            else
            {
                path = node.Tag.ToString();
            }

            lbl_scope.Text = "Selected Node: " + path;
            this.Update();
        }

        private void btn_copy_attr_Click(object sender, EventArgs e)
        {

            foreach (ListViewItem sel in listView1.SelectedItems)
            {
                if(!txt_attr.Lines.Contains(sel.Text))
                {
                    txt_attr.AppendText(sel.Text + Environment.NewLine);
                }

                sel.Selected = false;
            }

            this.Update();
        }

        private void treeView2_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

        }

    }
}
